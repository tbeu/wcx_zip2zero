// wcx_zip2zero.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
#include "cunicode.h"
#include "wcxhead.h" // WCX
#include <gsl/gsl_util>
#include <Poco/File.h>
#include <Poco/FileStream.h>
#include <Poco/Path.h>
#include <Poco/Timezone.h>
#include <Poco/TemporaryFile.h>
#include <Poco/UnicodeConverter.h>
#include <Poco/Zip/Compress.h>
#include <fstream>
#include <sstream>

// packer plugin function declarations
HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData);
HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData);
int __stdcall CloseArchive(HANDLE hArcData);
int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData);
int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData);
int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName);
int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName);
int __stdcall GetPackerCaps(void);
void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1);
void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1);
void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1);
void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1);
void __stdcall ConfigurePacker(HWND Parent, HINSTANCE DllInstance);
void __stdcall PackSetDefaultParams(PackDefaultParamStruct* dps);
void __stdcall PackSetDefaultParamsW(PackDefaultParamStructW* dps);
int __stdcall DeleteFiles(char *PackedFile, char *DeleteList);
int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList);
int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags);
int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags);

HINSTANCE hinst;
static tProcessDataProc pProcessDataProc;
static tChangeVolProc pChangeVolProc;
static tProcessDataProcW pProcessDataProcW;
static tChangeVolProcW pChangeVolProcW;

HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

int __stdcall CloseArchive(HANDLE hArcData) {
	return 0;
}

int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall DeleteFiles(char *PackedFile, char *DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall GetPackerCaps(void) {
	return PK_CAPS_NEW | PK_CAPS_MULTIPLE | PK_CAPS_OPTIONS | PK_CAPS_HIDE;
}

void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1) {
	pChangeVolProc = pChangeVolProc1;
}

void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1) {
	pChangeVolProcW = pChangeVolProc1;
}

void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1) {
	pProcessDataProc = pProcessDataProc1;
}

void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1) {
	pProcessDataProcW = pProcessDataProc1;
}

int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags) {
	char* AddList2 = AddList;
	while (*AddList) {
		AddList += strlen(AddList) + 1;
	}
	size_t s = AddList - AddList2 + 1;
	wchar_t* AddListW = new wchar_t[s];
	ZeroMemory(AddListW, s*(sizeof(wchar_t)));
	AddList = AddList2;
	wchar_t* AddListW2 = AddListW;
	while (*AddList) {
		awlcopy(AddListW, AddList, (int)strlen(AddList) + 1);
		AddListW += wcslen(AddListW) + 1;
		AddList += strlen(AddList) + 1;
	}

	wchar_t PackedFileW[wdirtypemax], SubPathW[wdirtypemax], SrcPathW[wdirtypemax];
	ZeroMemory(PackedFileW, wdirtypemax*(sizeof(wchar_t)));
	ZeroMemory(SubPathW, wdirtypemax*(sizeof(wchar_t)));
	ZeroMemory(SrcPathW, wdirtypemax*(sizeof(wchar_t)));

	int retVal = PackFilesW(awfilenamecopy(PackedFileW, PackedFile), awfilenamecopy(SubPathW, SubPath), awfilenamecopy(SrcPathW, SrcPath), AddListW2, Flags);
	delete[] AddListW;
	return retVal;
}

int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags) {
	int retVal{ 0 };

	if (nullptr != SubPath || nullptr == SrcPath) {
		return E_EWRITE;
	}

	if (AddList) {
		std::stringbuf buf; // empty buffer
		std::istream is{ &buf };
		const auto tzoff = Poco::Timestamp::TimeDiff(1e6) * (Poco::Timezone::utcOffset() + Poco::Timezone::dst());

		std::string utf8srcPath;
		Poco::UnicodeConverter::convert(SrcPath, utf8srcPath);
		Poco::Path srcPath{ utf8srcPath };
		std::string utf8targetPath;
		Poco::UnicodeConverter::convert(PackedFile, utf8targetPath);
		Poco::Path targetPath{ utf8targetPath };
		targetPath.setExtension("zip");
		// create zip as temporary and rename later to workaround issue with Unicode encoded file names
		Poco::TemporaryFile targetTempFile{ targetPath.parent().toString() };
		{
			Poco::FileOutputStream out{ targetTempFile.path(), std::ios::binary };
			if (!out.good()) {
				Poco::UTF16String wPath;
				Poco::UnicodeConverter::convert(targetTempFile.path(), wPath);
				MessageBoxW(nullptr, L"Bad output stream", wPath.c_str(), MB_OK | MB_ICONERROR);
				return E_ECREATE;
			}
			auto closeStream = gsl::finally([&out] { out.close(); });
			Poco::Zip::Compress compress{ out, true };
			auto closeZip = gsl::finally([&compress] { compress.close(); });

			// process AddList
			do {
				// get file fullPath
				Poco::Path fullPath{ srcPath };
				try {
					std::string utf8fileName;
					Poco::UnicodeConverter::convert(AddList, utf8fileName);
					Poco::Path fileName{ utf8fileName };
					if (!fileName.isAbsolute()) {
						fullPath.append(fileName);
					}
					else {
						fullPath = std::move(fileName);
						throw std::runtime_error("Unsupported use case.");
					}

					// pack to archive
					Poco::File file{ fullPath };
					if (fullPath.isFile()) {
						compress.addFile(is, file.getLastModified() + tzoff, utf8fileName);
						is.clear();
					}
					else if (fullPath.isDirectory()) {
						compress.addDirectory(utf8fileName, file.getLastModified() + tzoff);
					}

					// report progress
					if (pProcessDataProcW) {
						auto fSize{ file.getSize() };
						if (fSize > static_cast<Poco::File::FileSize>(INT32_MAX)) {
							const auto count{ static_cast<uint32_t>(fSize) / INT32_MAX };
							for (uint32_t i{ 0 }; i < count; ++i) {
								pProcessDataProcW(AddList, INT32_MAX);
							}
							fSize %= INT32_MAX;
						}
						if (0 == pProcessDataProcW(AddList, static_cast<int>(fSize))) {
							retVal = E_EABORTED;
							break;
						}
					}
				}
				catch (Poco::IOException& e) {
					Poco::UTF16String wPath, wMsg;
					Poco::UnicodeConverter::convert(fullPath.toString(), wPath);
					Poco::UnicodeConverter::convert(e.message(), wMsg);
					MessageBoxW(nullptr, wMsg.c_str(), wPath.c_str(), MB_OK | MB_ICONERROR);
					retVal = E_EWRITE;
					break;
				}
				catch (std::exception& e) {
					Poco::UTF16String wPath, wMsg;
					Poco::UnicodeConverter::convert(fullPath.toString(), wPath);
					Poco::UnicodeConverter::convert(e.what(), wMsg);
					MessageBoxW(nullptr, wMsg.c_str(), wPath.c_str(), MB_OK | MB_ICONERROR);
					retVal = E_EWRITE;
					break;
				}

				AddList += wcslen(AddList) + 1;
			} while (*AddList);
		}
		if (0 == retVal) {
			try {
				// rename zip file
				targetTempFile.moveTo(targetPath.toString());
				targetTempFile.keep();
			}
			catch (std::exception& e) {
				Poco::UTF16String wPath, wMsg;
				Poco::UnicodeConverter::convert(targetPath.toString(), wPath);
				Poco::UnicodeConverter::convert(e.what(), wMsg);
				MessageBoxW(nullptr, wMsg.c_str(), wPath.c_str(), MB_OK | MB_ICONERROR);
				retVal = E_EWRITE;
			}
		}
	}

	return retVal;
}
