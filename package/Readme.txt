Zip2Zero plugin 1.0.1.0 for Total Commander
===========================================

 * License:
-----------

Copyright (C) 2019-2021 by Thomas Beutlich

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.


 * Description:
---------------

Zip2Zero is a packer plugin for Total Commander to zip directory/file structures
as zero-byte files for convenient exchange of directory/file names.


 * ChangeLog:
-------------

 o Version 1.0.1.0 (29.05.2021)
   - fixed Unicode path issue
   - fixed error handling
 o Version 1.0.0.4 (07.07.2019)
   - fixed error handling
   - added progress bar
 o Version 1.0.0.3 (06.07.2019)
   - minor code improvements
 o Version 1.0.0.2 (03.07.2019)
   - fixed Unicode path issue
 o Version 1.0.0.1 (27.06.2019)
   - first public version


 * References:
--------------

 o POCO C++ Libraries: Foundation, Zip
   - https://github.com/pocoproject/poco
 o WCX Writer's Reference by Christian Ghisler & Jiri Barton
   - http://ghisler.fileburst.com/plugins/wcx_ref2.21se.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright © 1993-2021 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net